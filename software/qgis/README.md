# Installation of qgis

As per Giovanni's indications I'm installing qgis 3.4 (madeira).

These instruction from qgis blog don't work on my system (the repo is
not authenticated):
```
#wget -O - https://qgis.org/downloads/qgis-2019.gpg.key | gpg --import
#gpg --fingerprint 51F523511C7028C3	
```
Adding the source to ubuntu rep list, in /etc/apt/sources.list :
```
deb https://qgis.org/ubuntu-ltr/ bionic main      #for Ubuntu 18.04
deb-src https://qgis.org/ubuntu-ltr/ bionic main  #for Ubuntu 18.04

deb https://qgis.org/ubuntu-ltr/ xenial main      #for Ubuntu 16.04
deb-src https://qgis.org/ubuntu-ltr/ xenial main  #for Ubuntu 16.04
```

Adding the server key:
```
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 51F523511C7028C3 #for bionic
```

For xenial a different key number should be used (it's outputed when trying
to update the repos). However on Aug 2019 I tried and the keys are expired, thus 
the package cannot be authenticated.
	
And then I can install:
```
sudp apt-get update
sudo apt-get install qgis qgis-plugin-grass
```

qgis is now installed.

Note: to obtain qgis 3.x (madeira) we need at least ubuntu bionic (18.04).
Otherwise the installed version is 2.x (las palmas).

# Notes on qgis plugin

I've done a first survery of qgis plugins, and found nothing relative to machine/deep learning. In fact the relative tags are actually empty:

* [https://plugins.qgis.org/plugins/tags/deep-learning/](https://plugins.qgis.org/plugins/tags/deep-learning/)
* [https://plugins.qgis.org/plugins/tags/machine-learning/](https://plugins.qgis.org/plugins/tags/machine-learning/)

My hunch so far is that it will be difficult to actually integrate qgis with existing deep learning tools (Tensorflow, Torch/Pytorch, Keras...).

The only reference to machine/deep learning is on one promotional [feature page](https://geogeek.xyz/learning-qgis-3.html) but it's not clear what they mean.
