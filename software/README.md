# Data augmentation
* [tutorial](https://towardsdatascience.com/classify-butterfly-images-with-deep-learning-in-keras-b3101fe0f98) on data augmentation
* another [tutorial](https://divamgupta.com/image-segmentation/2019/06/06/deep-learning-semantic-segmentation-keras.html) from Divam Gupta that covers data segmentation

[Imgaug](https://github.com/aleju/imgaug) is a tool to perform image augmentation. Refer to the code snippet below which would apply Crop, Flip and GaussianBlur transformation randomly.

```python
import imgaug as ia
import imgaug.augmenters as iaa

seq = iaa.Sequential([
    iaa.Crop(px=(0, 16)), # crop images from each side by 0 to 16px (randomly chosen)
    iaa.Fliplr(0.5), # horizontally flip 50% of the images
    iaa.GaussianBlur(sigma=(0, 3.0)) # blur images with a sigma of 0 to 3.0
])

def augment_seg( img , seg  ):
	aug_det = seq.to_deterministic() 
	image_aug = aug_det.augment_image( img )

	segmap = ia.SegmentationMapOnImage( seg , nb_classes=np.max(seg)+1 , shape=img.shape )
	segmap_aug = aug_det.augment_segmentation_maps( segmap )
	segmap_aug =  segmap_aug.get_arr_int()

	return image_aug , segmap_aug
```

Here *aug_det* defines the parameters of the transformation, which is applied both to input image img and the segmentation image *seg*.

# Object detection

* [https://cv-tricks.com/object-detection/faster-r-cnn-yolo-ssd/] Overview of Faster R-CNN,YOLO,SSD

## YOLO
* [YOLO v3 in keras](https://machinelearningmastery.com/how-to-perform-object-detection-with-yolov3-in-keras/) : good for everything except the training (it loads a pretrained model). Many links at the bottom
* [Humble YOLO (v1?) implementation](https://blog.emmanuelcaradec.com/humble-yolo-implementation-in-keras/) : simple, trains on CPU, starts from synthetic data.
* [The YOLO source](https://pjreddie.com/darknet/yolo/) : they also have a lightweight section, and a tutorial on training. I should probably start here.

# Image segmentation

Online resources

* https://divamgupta.com/image-segmentation/2019/06/06/deep-learning-semantic-segmentation-keras.html
* from [this RG answer](https://www.researchgate.net/post/In_Keras_How_can_I_extract_the_exact_location_of_the_detected_object_or_objects_within_image_that_includes_a_background), we can also use non-DL image segmentation to simply extract each plant. Possible algos: watershed transformation or region growing
* to do the segmentation by hand: https://github.com/wkentaro/labelme
* dataset di esempio: [course.fast.ai/datasets](course.fast.ai/datasets) e cerca CAMVID (o altri, veh)
* the guys at fast.ai course released a pyTorch-based **Unet** architecture (fastai learner) which works really good on segmentation (see lesson https://course.fast.ai/videos/?lesson=3 ) - they also get the half precision floating point (*.to_fp16()* function)
* Unet in keras, [an example on ships](https://www.kaggle.com/krishanudb/keras-based-unet-model-construction-tutorial)
* Unet in keras, [an example on seismic data](https://www.depends-on-the-definition.com/unet-keras-segmenting-images/)
* Unet in keras, [an example on biological membrane data](https://github.com/zhixuhao/unet)




















