# Installation

Keras depends on some underlying engine. I stayed mainstream and went for TensorFlow, following the [instructions on Keras page](https://keras.io/#installation).

## Python 3 bits
Everything must be python3 ready, so ensure that:

```sh
python3 --version
pip3 --version
virtualenv --version
```

All reference to python version 3.4+, otherwise:

```sh
sudo apt update
sudo apt install python3-dev python3-pip

#this should install virtualenv system wide
#sudo -H pip3 install -U virtualenv  # system-wide install

#this is user-specific
pip3 install virtualenv

#and then install ensurepip
sudo apt-get install python3-venv

#And don't use Ubuntu repo's, they install virtualenv for python 2
#sudo apt install python-virtualenv #DON'T
```

## Making an ad-hoc virtual environment

In the proper location create and activate the virtual environment

```sh
cd ~/research/crea-za_maize_satellite/software/keras
python3 -m venv venv_keras
source venv_keras/bin/activate
```
I can now do:

```sh
(venv_keras) $ pip3 install <whatever>
```

## Installing TensorFlow

Reference: [tensorflow install pages](https://www.tensorflow.org/install/).

```sh
#must be inside venv_keras
pip install --upgrade pip
pip3 install tensorflow
```

## Installing cuDNN
Keras suggests using cuDNN if you plan to use Keras/TF on GPUs. However, right now I don't have any Nvidia graphic cards available. So, I'm just skipping this part.

Anyway, [instructions are here](https://docs.nvidia.com/deeplearning/sdk/cudnn-install/).

## Installing HDF5 and h5py
Keras suggests using HDF5 and h5py to store trained models on hard disk. 

h5py is a library for storing and managing big dataset directly from python, as numpy objects. It can become handy...

```sh
#must be inside venv_keras
#on my system it was already there...
pip3 install h5py
```

## Installing Keras
Nothing fancy here:

```sh
#must be inside venv_keras
pip3 install keras
```
# Using Keras 
## Warnings with TF + Numpy

Test a simple python script that imports keras:

```python
import keras
print ('hello')
```

If you execute it and obtain many warnings:
```sh
python3 ex1.py 
Using TensorFlow backend.
/home/nelson/research/crea-za_maize_satellite/software/keras/venv_keras/lib/python3.6/site-packages/tensorflow/python/framework/dtypes.py:516: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  _np_qint8 = np.dtype([("qint8", np.int8, 1)])
/home/nelson/research/crea-za_maize_satellite/software/keras/venv_keras/lib/python3.6/site-packages/tensorflow/python/framework/dtypes.py:517: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
<and many more>
```
It means there's a conflict between TF and numpy version, as [explained here](https://github.com/tensorflow/tensorflow/issues/30427)

I went with the easy solution and downgraded numpy:

```sh
pip3 install "numpy<1.17"
```
Obtaining:

```sh
python3 ex1.py 
Using TensorFlow backend.
hello
```
## Warnings with Keras + TF

Simply importing (and then using) Sequential models in keras generates many warnings, as:

```sh
WARNING:tensorflow:From /home/nelson/research/crea-za_maize_satellite/software/keras/venv_keras/lib/python3.6/site-packages/keras/backend/tensorflow_backend.py:1020: The name tf.assign is deprecated. Please use tf.compat.v1.assign instead.
```
And many more. It happens because Keras is using an old nomenclature for TF, now marked as obsolete. Not much I can do about it...

## RTFM (Keras docs)

* Keras Activation Functions: [https://keras.io/activations/](https://keras.io/activations/)
* Keras Models: [https://keras.io/models/about-keras-models/#about-keras-models](https://keras.io/models/about-keras-models/#about-keras-models)
* Keras Optimizers: [https://keras.io/optimizers/](https://keras.io/optimizers/)
* Keras Metrics: [https://keras.io/metrics/](https://keras.io/metrics/)
* Keras Datasets: [https/://keras.io/datasets/](https/://keras.io/datasets/)







