import keras
import pandas

#refs to doc
#Keras Activation Functions: https://keras.io/activations/
#Keras Models: https://keras.io/models/about-keras-models/#about-keras-models
#Keras Optimizers: https://keras.io/optimizers/
#Keras Metrics: https://keras.io/metrics/
#Keras Datasets: https/://keras.io/datasets/

#one of the two models in keras: the sequential
from keras.models import Sequential

#dense layers have all outputs from previous layer connected to 
#all inputs of next layer
from keras.layers import Dense

#invoking the constructor for current model
model = Sequential()

#importing concrete_data, to be studied
concrete_data = pandas.read_csv('https://ibm.box.com/shared/static/svl8tu7cmod6tizo6rk0ke4sbuhtpdfx.csv')
n_rows,n_cols = concrete_data.shape #the last column is target variable

#the split could be more random... :)
train_data = concrete_data.loc[0:799,:]
test_data  = concrete_data.loc[800:1029,:]

#predictors and variables columns
# partition the columns
features = concrete_data.columns[:-1]
target = concrete_data.columns[-1]

#some interface
print(concrete_data.head())
print ("Available data: " + str(n_rows) + " rows and " + str(n_cols) + " columns")

#building the net
model.add(Dense(5, activation='relu', input_shape=(n_cols - 1,))) #the last column is target variable
model.add(Dense(5, activation='relu'))
model.add(Dense(1)) #which activation here?

#defining how the net learns
#here "adam" is a strategy better suited than gradient descent for deep learning
#and it does not need the learning rate
model.compile(optimizer='adam', loss='mean_squared_error') 

#actually learning
model.fit(train_data.loc[:,features], train_data.loc[:,target])

#and now we can predict
predictions = model.predict(test_data.loc[:,features])

#here we should compute a sum of squared differences betwee predictions
#and test_data.loc[:,target] or any other error measure
