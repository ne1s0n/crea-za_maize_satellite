#these are notes, not a real runnable script. It discuss the
#general idea behind building a classifier in keras, as discussed in
#the course


import keras
import pandas
from keras.models import Sequential
from keras.layers import Dense
from keras.utils import to_categorical

#invoking the constructor for current model
model = Sequential()

#assuming that "target" contains a categorical numeric variable, 
#we need to explicitly convert it to classes (in R that would be factors)
target = to_categorical(target)

#three layers, two hidden (5 neurons each) plus the output layer
model.add(Dense(5, activation='relu', input_shape=(n_cols - 1,))) #the last column is target variable
model.add(Dense(5, activation='relu'))
model.add(Dense(4, activation='softmax')) #target has four classes -> four output neurons (each one binary)

#compiling the network
model.compile(optimizer='adam', 
	#loss='mean_squared_error',  #this won't work with categorical data
	loss='categorical_crossentropy',
	metrics=['accuracy']) #specify one or more metrics

#actually learning
model.fit(features, target, epochs=10)

#doing predictions
preds = model.predict(test_data)

#preds would be an array Nx4, where N is the number of data point in test_data
