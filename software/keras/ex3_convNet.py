#example from edx course. Comparing a simple network and a more
#structured convolutionary network 

#standar imports
import keras
from keras.models import Sequential
from keras.layers import Dense
from keras.utils import to_categorical

#special imports for convs
from keras.layers.convolutional import Conv2D # to add convolutional layers
from keras.layers.convolutional import MaxPooling2D # to add pooling layers
from keras.layers import Flatten # to flatten data for fully connected layers

#------------- DATA MANAGEMENT
#import data
from keras.datasets import mnist

# load data
(X_train, y_train), (X_test, y_test) = mnist.load_data()

# reshape to be [samples][pixels][width][height]
X_train = X_train.reshape(X_train.shape[0], 28, 28, 1).astype('float32')
X_test = X_test.reshape(X_test.shape[0], 28, 28, 1).astype('float32')

#normalize in [0,1]
X_train = X_train / 255 # normalize training data
X_test = X_test / 255   # normalize test data

#target variable to categorical
y_train = to_categorical(y_train)
y_test = to_categorical(y_test)

num_classes = y_test.shape[1] # number of categories

#------------- CNN SUPPORT FUNCTIONS
#Layers symbols:
# C  : convolutional layer
# MP : max pooling layer
# FC : fully connected layer
# O  : output layer

# C -> MP -> FC -> O
def convolutional_model_1(output_classes):
    # create model
    model = Sequential()
    
    #Convolutionary layer
    #adding 16 filters, dimension 5x5, with strides (1,1)
    #inputing images 28x28, one color channel
    model.add(Conv2D(16, (5, 5), strides=(1, 1), activation='relu', input_shape=(28, 28, 1)))
    
	#Pooling layer
	#doing max on a 2x2 grid
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    
    #flatten -> data becomes 1D array
    model.add(Flatten())
    
    #fully connected layer
    model.add(Dense(100, activation='relu'))
    
    #output layer
    model.add(Dense(output_classes, activation='softmax'))
    
    # compile model
    model.compile(optimizer='adam', loss='categorical_crossentropy',  metrics=['accuracy'])
    return model

# C -> MP -> C -> MP -> FC -> O
def convolutional_model_2(output_classes):
    # create model
    model = Sequential()
    model.add(Conv2D(16, (5, 5), activation='relu', input_shape=(28, 28, 1)))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    
    model.add(Conv2D(8, (2, 2), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    
    model.add(Flatten())
    model.add(Dense(100, activation='relu'))
    model.add(Dense(output_classes, activation='softmax'))
    
    # Compile model
    model.compile(optimizer='adam', loss='categorical_crossentropy',  metrics=['accuracy'])
    return model

#------------- DOING THE DEED
# build the models
model_1 = convolutional_model_1(num_classes)
model_2 = convolutional_model_2(num_classes)

# fit the models
model_1.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=10, batch_size=200, verbose=2)
model_2.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=10, batch_size=200, verbose=2)

# evaluate the models
scores_1 = model_1.evaluate(X_test, y_test, verbose=0)
scores_2 = model_2.evaluate(X_test, y_test, verbose=0)
print("FIRST NETWORK")
print("Accuracy: {} \n Error: {}".format(scores_1[1], 100-scores_1[1]*100))
print("\nSECOND NETWORK")
print("Accuracy: {} \n Error: {}".format(scores_2[1], 100-scores_2[1]*100))

#------------- RESULTS
#FIRST NETWORK
#Accuracy: 0.9879 
# Error: 1.2099999999999937

#SECOND NETWORK
#Accuracy: 0.9871 
# Error: 1.2900000000000063

#So... first network, simpler, is better :) It's probably a problem
#of overfitting.
