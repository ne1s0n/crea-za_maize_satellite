#following the tutorial here: https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/DL0101EN/DL0101EN-3-2-Classification-with-Keras-py-v1.0.ipynb

import keras

from keras.models import Sequential    #network model
from keras.layers import Dense         #layer structure
from keras.utils import to_categorical #utility function

#since we use images
import matplotlib.pyplot as plt

#The Keras library conveniently includes the MNIST dataset as part of 
#its API. You can check other datasets within the Keras at https/://keras.io/datasets/

#MNIST: 70000 images of hand written digits, already divided in train
#and test.

# import the data
from keras.datasets import mnist #at the first execution data are downloaded

# read the data
(X_train, y_train), (X_test, y_test) = mnist.load_data()

#how many data do we have?
print(X_train.shape)
print(X_test.shape)
#(60000, 28, 28)
#(10000, 28, 28)

#The first number in the output tuple is the number of images, 
#and the other two numbers are the size of the images in datset. 
#So, each image is 28 pixels by 28 pixels.

#let's look at one image
plt.imshow(X_train[0]) #I'm not sure what is supposed to happen here...

#let's flatten the image so it becomes a vector feedable to the network

# flatten images into one-dimensional vector
num_pixels = X_train.shape[1] * X_train.shape[2] # find size of one-dimensional vector
X_train = X_train.reshape(X_train.shape[0], num_pixels).astype('float32') # flatten training images
X_test = X_test.reshape(X_test.shape[0], num_pixels).astype('float32') # flatten test images

# normalize inputs from 0-255 to 0-1
X_train = X_train / 255
X_test = X_test / 255

#make output categorical (one hot encoding)
y_train = to_categorical(y_train)
y_test = to_categorical(y_test)
num_classes = y_test.shape[1]
print("Number of classes: " + str(num_classes))

#Support function: define classification model
#I've added the number of input pixels and of output classes as
#function parameters (to avoid reading num_pixels and num_classes
#directly from out-of-function scope)
def classification_model(input_pixels, output_classes):
    # create model: 
    #- input layer (num pixels)
    #- one hidden layers (100 neurons)
    #- output layer (one neuron per class)
    model = Sequential()
    model.add(Dense(input_pixels, activation='relu', input_shape=(input_pixels,)))
    model.add(Dense(100, activation='relu'))
    model.add(Dense(output_classes, activation='softmax'))
    
    # compile model
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    return model

#DOING THE DEED

# build the model
model = classification_model(num_pixels, num_classes)

# fit the model
model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=10, verbose=2)

# evaluate the model
scores = model.evaluate(X_test, y_test, verbose=0)

# save the model for future uses
model.save('classification_model.h5')

#for future use
from keras.models import load_model
pretrained_model = load_model('ex2b_classification_model.h5')
