library(madbito)
setwd('~/research/crea-za_maize_satellite/data/')


# DATA IN -----------------------------------------------------------------
all.samples = read.csv('batch_01_public/classi/resulting_classes.csv', stringsAsFactors = FALSE)

# BUILDING MORE BALANCED SETS ---------------------------------------------
#the original sets are a bit unbalanced: there are more validation than calibration
#samples! Moreover classes proportions are a bit different (there are way less class 4
#in validation set). I decide to create a different division, using a fifth of the
#total samples for validation and respecting the classes proportion
folds.num = 5
folds = stratified_crossvalidation_folds(all.samples$Class, folds.num = folds.num)

for (fold.current in 1:folds.num){
  writeLines(paste('---------- doing fold', fold.current))
  calibration.new = all.samples[folds != fold.current,]
  validation.new = all.samples[folds == fold.current,]
  
  writeLines('Calibration proportions')
  print(table(calibration.new$Class)/nrow(calibration.new))
  writeLines('Validation proportions')
  print(table(validation.new$Class)/nrow(validation.new))
  nval = nrow(validation.new)
  ntot = nrow(all.samples)
  writeLines(paste(sep = '', 'validation: ', nval, '/', ntot, ' (', nval/ntot, ')'))
  for (i in 1:nrow(calibration.new)){
    #destination
    outdir = paste(sep='', 'batch_01_private/quadrati 5m_preproc01.balanced/fold_', fold.current, '/train/', calibration.new[i,]$Class)
    
    #making sure the folder is there
    dir.create(outdir, showWarnings = FALSE, recursive = TRUE)
    
    #copying the data
    infile = paste(sep='', 'batch_01/quadrati 5m_preproc01/', calibration.new[i,]$File)
    file.copy(to = outdir, from = infile, overwrite = TRUE)  
  }
  for (i in 1:nrow(validation.new)){
    #destination
    outdir = paste(sep='', 'batch_01_private/quadrati 5m_preproc01.balanced/fold_', fold.current, '/valid/', validation.new[i,]$Class)
    
    #making sure the folder is there
    dir.create(outdir, showWarnings = FALSE, recursive = TRUE)
    
    #copying the data
    infile = paste(sep='', 'batch_01/quadrati 5m_preproc01/', validation.new[i,]$File)
    file.copy(to = outdir, from = infile, overwrite = TRUE)  
  }
  
  rm(i, infile, outdir)
  
  #if we get here it's time to create compressed archives
  setwd('batch_01_private/quadrati 5m_preproc01.balanced/')
  cmd = paste(sep='', 'tar -czvf batch02_fold', fold.current, '.tar.gz fold_', fold.current)
  system(cmd)
  setwd('../..')
}