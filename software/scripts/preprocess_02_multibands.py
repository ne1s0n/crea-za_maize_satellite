import sys, numpy, imageio, os, glob, scipy
from mylib import imgtoolbox
from scipy import ndimage, misc


infolder = "/home/nelson/research/crea-za_maize_satellite/data/batch_04_multispettrale_private/volo_15062019_5bande"
#infolder = sys.argv[1]

#should we reduce to three channels (Red, Red Edge, Near Infrared)
#or keep all five of them? (Pick one)
#three_channels = True
three_channels = False

#should we output jpg? (values must be scaled in 0-255 range)
#output_jpg = True
output_jpg = False

#this is the maximum value originally found all over the dataset (which is 
#supposed to be in [0,1] range). This is to stretch the available range when 
#rescaling values.
max_value = 0.61

#----------- ACTUAL SCRIPT
#range for rescaling
output_range =  65536
if output_jpg:
	output_range = 255

#deriving outfolder
outfolder = infolder.rstrip('/')+"_preproc01/"
if three_channels:
	outfolder = infolder.rstrip('/')+"_preproc01_3bands/"

#making room for results
if not os.path.exists(outfolder):
	os.makedirs(outfolder)

#imageset max and min
all_max = float('-Inf')
all_min = float('Inf')

#scanning all .tif from infolder
img_cnt = 0
imgs = glob.glob(infolder + '/*.tif')
for img_file in imgs:
	img_cnt += 1
	print('Doing image ' + img_file + ' (' + str(img_cnt) + ' of ' + str(len(imgs)) + ')')
	img = imageio.imread(img_file)
	
	#interface
	print("Original shape: " + str(img.shape) + ' Range: [' + str(numpy.amin(img)) + ', ' + str(numpy.amax(img)) + ']')
	
	#taking into account global max and min
	img_min = numpy.amin(img)
	img_max = numpy.amax(img)
	if all_max < img_max:
		all_max = img_max 
	if all_min > img_min:
		all_min = img_min
		
	#rescaling to allow visualization (from previous passes
	#I know that max value found is about 0.61)
	img = img * output_range / 0.61
	
	#interface
	print("Rescaled shape: " + str(img.shape) + ' Range: [' + str(numpy.amin(img)) + ', ' + str(numpy.amax(img)) + ']')
	
	#at this point the image should be in the [0-output_range] interval
	#but just to check...
	if (numpy.amin(img) < 0) or (numpy.amax(img) > output_range):
		raise Exception('Image range out of [0-' + str(output_range) + '] boundaries')
	
	#should we reduce to three channels?
	if three_channels:
		#band order:
		#channel 0: B
		#channel 1: G 
		#channel 2: Ni 
		#channel 3: R 
		#channel 4: Re
		img = img[:,:,::2] #selecting B Ni Re
	
	#should we output jpg?
	if output_jpg:
		#converting to jpg range
		img = img.astype(numpy.uint8)
	else:
		#convertying to tif integer range
		img = img.astype(numpy.uint16)

	#interface
	print("Pre rotation shape: " + str(img.shape) + ' Range: [' + str(numpy.amin(img)) + ', ' + str(numpy.amax(img)) + ']')

	#rotating
	#img = ndimage.rotate(img, -11.141)
	img = ndimage.rotate(img, -11.2, order=1)
	
	#interface
	print("Post rotation shape: " + str(img.shape) + ' Range: [' + str(numpy.amin(img)) + ', ' + str(numpy.amax(img)) + ']')

	#removing the outside border.
	#with several successive approximations, I found that this is
	#the best square
	#img = imgtoolbox.draw_rectangle(img, 0, 0, height-1, width-1, color = (255, 255, 255))
	#img = imgtoolbox.draw_rectangle(img, 31, 31, 188, 188, color = (255, 255, 255))
	img = img[31:188,31:188,:]

	#interface
	print("Post cropping shape: " + str(img.shape) + ' Range: [' + str(numpy.amin(img)) + ', ' + str(numpy.amax(img)) + ']')

	#at this point the image should be in the [0-output_range] interval
	#but just to check...
	if (numpy.amin(img) < 0) or (numpy.amax(img) > output_range):
		raise Exception('Image range out of [0-' + str(output_range) + '] boundaries')
	
	#saving
	outfile = outfolder + os.path.basename(img_file)[:-4]
	if output_jpg:
		outfile += '.jpg'
		imageio.imwrite(outfile, img, quality = 100)
	else:
		#each band is saved in a separate image
		for i in range(5):
			outfile_curr = outfile + '_channel' + str(i) + '.tif'
			imageio.imwrite(outfile_curr, img[:, :, i])

#final interface
print('Total range: [' + str(all_min) + ', ' + str(all_max) + ']')
