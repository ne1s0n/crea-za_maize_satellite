setwd('~/research/crea-za_maize_satellite/data/batch_01_public/classi/')


# DATA IN -----------------------------------------------------------------
calibration = rbind(
  read.csv('raw/classes cal_1_nic.csv', stringsAsFactors = FALSE),
  read.csv('raw/classes cal_2_nic.csv', stringsAsFactors = FALSE),
  read.csv('raw/classes cal_3_nic.csv', stringsAsFactors = FALSE),
  read.csv('raw/classes cal_4_nic.csv', stringsAsFactors = FALSE),
  read.csv('raw/classes cal_5_nic.csv', stringsAsFactors = FALSE),
  read.csv('raw/classes cal_6_nic.csv', stringsAsFactors = FALSE)
)

validation = rbind(
  read.csv('raw/classes val_1_nic.csv', stringsAsFactors = FALSE),
  read.csv('raw/classes val_2_nic.csv', stringsAsFactors = FALSE),
  read.csv('raw/classes val_3_nic.csv', stringsAsFactors = FALSE),
  read.csv('raw/classes val_4_nic.csv', stringsAsFactors = FALSE),
  read.csv('raw/classes val_5_nic.csv', stringsAsFactors = FALSE)
)

full = rbind(calibration, validation)

#max(table(calibration$File)) #one
#max(table(validation$File)) #one

table(calibration$Class)
# 1 - no infestation 2 - some infestation 3 - high infestation        4 - no plants 
# 578                  410                  389                   97 

table(validation$Class)
# 1 - no infestation 2 - some infestation 3 - high infestation        4 - no plants 
# 826                  381                  251                   27 

table(full$Class)
# 1 - no infestation 2 - some infestation 3 - high infestation        4 - no plants 
# 1404                  791                  640                  124 



# DATA OUT ----------------------------------------------------------------
write.csv(calibration, 'calibration.csv', row.names = FALSE)
write.csv(validation, 'validation.csv', row.names = FALSE)
write.csv(full, 'full.csv', row.names = FALSE)

