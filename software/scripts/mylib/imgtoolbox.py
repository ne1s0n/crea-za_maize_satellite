from skimage.draw import line_aa #pip3 install scikit-image

def draw_line(img, x1, y1, x2, y2, color = (0, 255, 0)):
	rr, cc, val = line_aa(x1, y1, x2, y2)
	img[rr, cc, :] = color
	return(img)
	
def draw_rectangle(img, x1, y1, x2, y2, color = (0, 255, 0)):
	img = draw_line(img, x1, y1, x1, y2, color)
	img = draw_line(img, x1, y1, x2, y1, color)
	img = draw_line(img, x1, y2, x2, y2, color)
	img = draw_line(img, x2, y1, x2, y2, color)
	
	return(img)
