#this script extracts from all the .tif found in the passed folder and:
#-subtitutes the value -32768 (tif transparent) with zeros
#-rotates of -11.141° (number given, is fix for this field)

#import sys, os, glob
#import numpy, scipy, imageio

import sys, numpy, imageio, os, glob, scipy
from mylib import imgtoolbox
from scipy import ndimage, misc

if len(sys.argv) == 1:
	print("You need the pass the picture folder")
	exit()

infolder = sys.argv[1]
outfolder = sys.argv[1].rstrip('/')+"_preproc01/"

#making room for results
if not os.path.exists(outfolder):
	os.makedirs(outfolder)

#scanning all .jpg from infolder
imgs = glob.glob(infolder + '/*.tif')
for img_file in imgs:
	print('Doing image ' + img_file)
	img = imageio.imread(img_file)
	(height, width, channels) = img.shape
	
	#if we have more than 3 channels we focus on the first three
	if len(img[0][0]) > 3:
		#print("More than three channels! Keeping the first three\n")
		img = img[:, :, 0:3]
	
	#substituting problematic value (-32768) with a tamer one
	img = numpy.where(img == -32768, (255,255,255), img)
	
	#at this point the image should be in the [0-255] range, which
	#is ok for a uint8 (jpg). But let's just check
	img_min = numpy.amin(img)
	img_max = numpy.amax(img)
	if (img_min < 0) or (img_max > 255):
		raise Exception('Image range bigger than output range: [' + str(img_min) + ', ' + str(img_max) + ']')
	
	#converting to jpg range
	img = img.astype(numpy.uint8)
	
	#rotating
	#img = ndimage.rotate(img, -11.141)
	img = ndimage.rotate(img, -11.2)
	
	#removing the outside border.
	#with several successive approximations, I found that this is
	#the best square
	#img = imgtoolbox.draw_rectangle(img, 82, 82, 496, 496)
	#img = imgtoolbox.draw_rectangle(img, 57, 57, 352, 352, color = (255, 0, 255))
	img = img[57:352,57:352,:]

	#saving
	outfile = outfolder + os.path.basename(img_file)[:-4] + '.jpg'
	imageio.imwrite(outfile, img, quality = 100)
