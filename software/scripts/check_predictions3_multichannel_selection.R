library(ggplot2)
setwd('~/research/crea-za_maize_satellite/')

NET = 'b1'
RS=42
#channel 0: B
#channel 1: G 
#channel 2: Ni 
#channel 3: R 
#channel 4: Re
bands_labels = c('B','G','Ni','R','Re')

get_conf = function(conf_number){
  #getting a TRUE/FALSE representation
  binary = intToBits(conf_number)[1:5] == 01
  names(binary) = bands_labels
  
  #building a string representation
  hash_human = gsub(pattern = '.', replacement = '_', bands_labels)
  hash_human[binary] = bands_labels[binary]
  
  #we produce also a hash with the standard order
  #with blue on the right
  hash = hash_human[5:1]
  hash = paste(collapse = '', hash)
  hash_human = paste(collapse = '', hash_human)
  
  return(list(
    binary=binary,
    hash=hash,
    hash_human=hash_human
  ))
}

res = NULL
for (i in 1:31){
  #getting the string representation
  conf = get_conf(i)
  
  #looking for available files
  pattern = paste(sep='', 
                  'channelComparisons_predictions_channels', conf$hash,
                  'NET', NET,
                  '_epochs20_folds5_',
                  'RS', RS, 
                  '_fold'
                  )
  all_files = list.files(
            path = 'results/run08/multispettrale_channel_selection/raw_preds/', 
            full.names = TRUE, 
            pattern = pattern)
  
  #if nothing is here, let's skip
  if (length(all_files) == 0){
    next()
  }
  
  #collecting all predictions
  all_preds = NULL
  for (file_now in all_files){
      all_preds = rbind(all_preds, read.csv(file_now, stringsAsFactors = FALSE))
  }
  
  #computing accuracy
  acc = sum(all_preds$pred_label == all_preds$class_label) / nrow(all_preds)
  
  #extracting average LR
  LR = unique(all_preds$extra)
  LR = as.numeric(gsub(LR, pattern = 'selected_LR=', replacement = ''))
  LR = mean(LR)
  
  #storing results
  res = rbind(res, data.frame(
    conf = conf$hash_human,
    acc = acc,
    error = 1 - acc,
    LR = LR,
    t(conf$binary)
  ))
}

#doing a nice ranking
res = res[order(res$acc, decreasing = TRUE),]
res$rank = 1:nrow(res)
res$inverse_rank = nrow(res):1

#doing a ranking of the channels
bands_rank = c(0,0,0,0,0)
names(bands_rank) = bands_labels
for (i in 1:nrow(res)){
  sel = as.logical(res[i,bands_labels])
  bands_rank[sel] = bands_rank[sel] + res[i,]$inverse_rank 
}

#printing ranks in a markdown ready format
writeLines(paste(collapse=' | ', names(bands_rank)))
writeLines(paste(collapse=' | ', rep('-', length(bands_rank))))
writeLines(paste(collapse=' | ', bands_rank))

#saving everything
write.csv('results/run08/multispettrale_channel_selection/conf_ranking.csv', x = res, row.names = FALSE)
write.csv('results/run08/multispettrale_channel_selection/band_ranking.csv', x = bands_rank)

#building a plot
res$conf = factor(res$conf, levels = res$conf)
g = ggplot(res, aes(x=conf, y=error)) + geom_point() + 
  theme(axis.text.x = element_text(angle = -90, vjust = 0, color = 'black')) +
  xlab('') + ylab('Error rate') + ggtitle(paste('Channel selection comparison - NET', NET))
g
ggsave('results/run08/multispettrale_channel_selection/conf_ranking.png', g)