#this script extracts from all the .jpg found in the passed folder
#a core section (the central part), tassellates it in many smalle images,
#and saves everything in the new folder _core

#how much the core measures, with respect of the original image
CORE_FRACTION = 0.25
#in how many pieces must the core be broken
CORE_STEPS = 10

import sys, os, glob
import numpy, scipy, imageio

if len(sys.argv) == 1:
	print("You need the pass the picture folder")
	exit()

infolder = sys.argv[1]
outfolder = sys.argv[1].rstrip('/')+"_core/"

#making room for results
if not os.path.exists(outfolder):
	os.makedirs(outfolder)

#scanning all .jpg from infolder
imgs = glob.glob(infolder + '/*.jpg') + glob.glob(infolder + '/*.JPG')
for img_file in imgs:
	print('Doing image ' + img_file)
	img = imageio.imread(img_file)
	(height, width, channels) = img.shape
	
	#computing the core dimensions and starting corner coordinates
	core_w = int(width * CORE_FRACTION)
	core_h = int(height * CORE_FRACTION)
	core_x = int((width - core_w) / 2)
	core_y = int((height - core_h) / 2)
	
	#the core is, itself, tassellated
	step_x = int(core_w / CORE_STEPS)
	step_y = int(core_h / CORE_STEPS)
	
	for i in range(0,CORE_STEPS):
		for j in range(0,CORE_STEPS):
			x = core_x + i * step_x
			y = core_y + j * step_y
			
			#slicing the image
			img_core = img[y:(y+step_y), x:(x+step_x),:]
			
			#building the output filename
			outfile = outfolder + os.path.basename(img_file)[:-4]
			outfile += '_' + str(i) + '_' + str(j) + '.jpg'
			
			#saving
			imageio.imwrite(outfile, img_core)
