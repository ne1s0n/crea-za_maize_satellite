# Aim
Automatic detection of pest plants in field. Input data: drone RGB and multispectral field pictures. Techniques: machine-learning and deep-learning. Status: work in progress.

# People
We are a group of researcher in CREA-ZA, Lodi. This repo is maintained by me 
([@Ne1s0n](https://gitlab.com/ne1s0n)) and documents the development of the project.

# Diary

## 2020-04-16 Run 09 Efficientnet on subsets of the 5-bands set

Given the five bands (B G Ni R Re) we can turn on and off each channel and obtain 32 (2^5) possible combinations. Out of these one is not meaningful (all channels off) and will be skipped. I tested the remaining 31 combinations to understand if there is a better configuration to be used other than "all on".

The procedure is very similar to what I did before (5 folds crossvalidation on all field, choice of optimal LR) and brought to this ranking based on error rates:

![alt text](results/run08/multispettrale_channel_selection/conf_ranking.png "Channel selection comparison")

I've also created a band ranking: all bands in the best configuration of the accuracy ranking received 31 points, all bands in the second-best one received 30 points and so forth. This resulted in the following points:

B   |   G |  Ni |   R | Re
----|-----|-----|-----|----
279 | 327 | 221 | 247 | 278


## 2020-04-16 Run 08 Efficientnet on new dataset (voloAgosto)

I've tested Efficientnet on the new RGB dataset with grown plants. As expected, the data are much more difficult to predict, bringing to worse performances:

Dataset    | Net | Plants error | Dose error | Dose correlation
-----------|-----|--------------|------------|------------------
RGB agosto | b0  | 0.248 | 0.246 | 0.789
RGB agosto | b1  | 0.251 | 0.250 | 0.786

Please note that "Plants error" is calculated on four classes since with grown plants there's no more class 5_dicotyledon visible.

## 2020-04-08 Run 08 Efficientnet on both RGB and 5-bands sets

I've completed the run of Efficentnet (b0 and b1) on both RGB and 5-bands sets. In both cases it is confirmed that RGB performs better. It may be due to bigger image size (414 vs 224) or that having 5 channels complicates too much the problem.

Moreover, we are now working on a 5 classes problem: 1_no_infestation, 2_some_infestation, 3_high_infestation, 4_no_plants, 5_dicotyledon. I worked under the hypothesis that classes 2 and 3 refer to Johnson grass (Sorghum halepense, *sorghetta*).

This is our current best performances:

Dataset | Net | Plants error | Dose error | Dose correlation
--------|-----|--------------|------------|------------------
multispettrale | b0 | 0.137 | 0.134 | 0.825
multispettrale | b1 | 0.124| 0.122 | 0.836
RGB | b0 | 0.101| 0.100 | 0.869
RGB | b1 | 0.091 | 0.090 | 0.879

Where:

* **Plants error** is the fraction of wrongly classified plants in the 5 classes schema
* **Dose error** is the fraction of wrong classification when predicting the dose of herbicide (no_infestation=0, some_infestation=1, high_infestation=2, no_plants=0, dicotyledon=1.5)
* **Dose correlation** is Pearson's correlation computed on herbicide doses

Predictions of the full field (produced in the usual 5-folds stratified crossvalidation) [are found here](results/run08/)

I've also compared the two best predictions for 5-bands and RGB, finding that they differ in the 11.7% of the predictions. This can be considered a clue that there is no polarization in the errors. In other words, the errors are evenly distributed among samples. A high coherence of the two predictions would mean that there are some samples where the network is dead sure, and which are wrong.

In particular, the breakdown of the predictions is the following:

never_predicted | always_predicted | RGB_only | MULTI_only
----------------|------------------|----------|------------
0.051           | 0.836            | 0.073    | 0.040

## 2020-02-19 Run 05c Efficientnet on RGB set

Given the improvement on multispectral images I've repeated [Run05](https://gitlab.com/ne1s0n/crea-za_maize_satellite#2020-01-30-run-05b-results-and-effect-of-data-augmentation-and-image-resizing) using EfficientNet. I report the previous results and the new one for comparison:

Net | Conf | Size | Plants error | Dose error | Dose correlation
----|-----|------|---------------|------------|-------------------
Resnet34 | full  | 414 | 0.218 | 0.192 | 0.842
Resnet34 | full | 224 | 0.206 | 0.189 | 0.844
Resnet34 | noFlip_noRot | 414 | 0.213 | 0.192 | 0.843
Resnet34 | noFlip_noRot | 224 | 0.210 | 0.190 | 0.838
**EfficientNet** | full | 224 | 0.084 | 0.077 | 0.934

There is thus a clear improvement on performances, both for Plant based classification (4 classes) and Dose based classification (3 classes). 

The confusion matrix for Plant classification (as usual, rows are true values, columns are predicted values):

. | 1_no_infestation | 2_some_infestation | 3_high_infestation | 4_no_plants
--|------------------|--------------------|--------------------|------------
1_no_infestation           |    1350        |         53 |                 2  |         6
2_some_infestation         |      91        |        660 |                43  |         0
3_high_infestation         |       2        |         31 |               603  |         0
4_no_plants                |      15        |          0 |                 5  |        98

and for Doses classification: 

. | 0  | 1 |  2
--|----|---|-----
0 | 1469 |  53 |  7
1 |   91 | 660 | 43
2 |    2 |  31 | 603

TODO:

* proper standardized comparison (repeated crossvalidation) for ResNet and EfficientNet on both dataset 1 and 2

## 2020-02-18 Run 07bis multispectral images - more tests

I've replicated the experiment of the previous run but comparing Doses and Plant Type. This round I've tested four configs::

Net             | LR    | Plant error   | Dose error 
----------------|-------|-----|------------
EfficientNet-B0 | 10e-2 | 0.196 | 0.172
EfficientNet-B0 | 10e-3 | 0.210 | 0.178
EfficientNet-B1 | 10e-2 | 0.220 | 0.190
EfficientNet-B1 | 10e-3 | 0.188 | 0.201

As expected the Dose Errors are lower than Plant Errors, given that the problem is simpler (3 classes vs. 4 classes).

Note: this was a short test (10 epochs) looking at the evolution plots of errors and other params I have the feeling that longer runs could lead to further improvements.

## 2020-02-05 Run 07 multispectral images - EfficientNet

I've prepared a [preprocessing script](software/scripts/preprocess_02_multibands.py) that reads in the 5-channels multiband data, does the standard rotation+cropping, and saves the 5 separate channels as b/w tiff without losing data range representation power. An [example is available](data/batch_04_multispettrale/example/) with an original multichannel image plus the resulting five images. Due to the library used for rotation ([scipy.ndimage](https://docs.scipy.org/doc/scipy/reference/ndimage.html)) the range of the resulting rotated image is wider *after rotation*. This can create further uncertainty down the processing pipeline. An alternative could be to rotate the images using QGIS so that I don't need to rotate them in python. As a side node, to this batch of images I applied a rotation of -11.2 degrees.

I then proceeded to test the multispectral images with the CNN. I followed the skeleton from [this kaggle script](https://www.kaggle.com/tanlikesmath/rcic-fastai-starter) on how to extend a fastai network to more channels. I was however unable to work with the old network architecture (Resnet34) and I ended up adopting a new one named EfficientNet ([paper](https://arxiv.org/abs/1905.11946)) ([implementation](https://github.com/tensorflow/tpu/tree/master/models/official/efficientnet)). I've tried (and am trying) different configurations. In so far my best result is:

* architecture: EfficientNet b0
* learning rate: 10e-2
* error rate: 17.02 %

Results were obtained on a random split 80/20 split for training/test. Please note that this is probably close to the best that we can expect. For reference, I've measure the "error rate" of the two hand classifications, as if one were the true values and the other the predicted values. The result is an error rate of 25.50 % In other words, the neural network is already doing sensibly better than the hand classification :)

Next steps:

* test with classes 4 and 1 separated
* test EfficientNet on regular RGB data (since it does so much better than Resnet)
* tweak EfficientNet params
* adapt the script to produce predictions on the entirety of the field

## 2020-01-30 Run 05b results and effect of data augmentation and image resizing
I've tested the data from the previous update and explored two parameters:

* image resizing: images by default are resized from 414x414 to 224x224 for speed of computation purposes
* image augmentation, in particular from fastai [get_transform()](https://docs.fast.ai/vision.transform.html#get_transforms) defaults I've tested the effect of turning off image flipping and image rotations

Here's the results 

Conf | Size | Class error | Dose error | Dose correlation
-----|------|---------------|------------|-------------------
full  | 414 | 0.218 | 0.192 | 0.842
full | 224 | 0.206 | 0.189 | 0.844
noFlip_noRot | 414 | 0.213 | 0.192 | 0.843
noFlip_noRot | 224 | 0.210 | 0.190 | 0.838

Where:

* Conf, Size are the considered configuration
* Class error is the total error rate, in percentage, when trying to identify the four classes
* Dose error is the total error rate when classes 0 (no infestation) and 4 (no plants) are considered the same, so that the classes become: 0 (no herbicide needed), 1 (low dose of herbicide) and 2 (high dose of herbicide)
* Dose correlation is the Pearson's correlation between the dose values (true and predicted)

There is little effect in general, with full augmentation + image resizing to 224 giving slightly better results.


## 2020-01-23 Run 05b - Multi pass classification

In an effort to improve the quality of ground truth we produced a second (and, for same samples, third) pass of manual classification. Classification data now follow these arbitration rules:

* the [40 ambiguous data from run one](https://gitlab.com/ne1s0n/crea-za_maize_satellite#2019-10-30-run02-classification-updates) are kept as updated true values
* when two classifications are present the most recent one wins
* when three classifications are present we implement a majority rule

This resulted in the following situation:

*Before arbitration*

first | first_update | second | third
------|--------------|--------|----------------
 3290 |          40  |   3290 |          296 

*After arbitration*

majority | second_wins 
---------|--------------
296      | 2994

## 2020-01-16 Run 05 - Predicting the field

I've produced the predictions for the whole field using a 5-fold crossvalidation, i.e. I've used 80% of the field to predict the remaining 20%. Rotating the process 5 times I've thus produced predicitons for the whole field. The results are [collected here](results/run05/fold_all_preds.csv)


## 2019-12-18 State of the art

I review some papers to have an idea about the current discourse on the topic.

### [* Automatic expert system based on images for accuracy crop row detection in maize fields (2013)](https://www.sciencedirect.com/science/article/pii/S0957417412009293)

This paper is about row detection. It uses a two steps approach: image binaryzation and row estimation. The first step is of interest for us: they normalize the image and then derive a vegetation index (per pixel, easily computed). They then use the index to do greenness reinforcement, so that the successive Otsu thresholding is very effective in separating vegetation (white pixels) from soil (black pixels).

The second step is more about optimizing the coefficients of first order equations so that the resulting line approximates best the white pixels, and demonstrate that Theil-Sen estimator is better than Pearson's correlation.

### [* Automatic expert system for weeds/crops identification in images from maize fields (2012)](https://www.sciencedirect.com/science/article/pii/S0957417412008895)

This paper is about identification of "masked" plants, that can be either parts of plants partially covered in soil or plants that are losing their greenness (after herbicide or other detrimental causes).

They compute a vegetation index (easy) and apply Otsu thresholding to separate unmasked vegetation (white pixels) from soil + masked vegetation (black pixels). They then apply Otsu thresholding a second time to black pixel only and obtain a set of masked plants. They then apply a cleanup factor to remove small patches and isolated pixels, under the assumption that plants come in lumps.

### To be read

* [A review on weed detection using ground-based machine vision and image processing techniques (2019)](https://www.sciencedirect.com/science/article/pii/S0168169918317150)
* [Fully Convolutional Networks With Sequential Information for Robust Crop and Weed Detection in Precision Farming (2018)](https://ieeexplore.ieee.org/abstract/document/8379414)
* [Real-Time Semantic Segmentation of Crop and Weed for Precision Agriculture Robots Leveraging Background Knowledge in CNNs (2018)](https://ieeexplore.ieee.org/abstract/document/8460962)
* [A fully convolutional network for weed mapping of unmanned aerial vehicle (UAV) imagery (2018)](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0196302)
* [Fast and Accurate Crop and Weed Identification with Summarized Train Sets for Precision Agriculture (2017)](https://link.springer.com/chapter/10.1007/978-3-319-48036-7_9)
* [weedNet: Dense Semantic Weed Classification Using Multispectral Images and MAV for Smart Farming (2017)](https://ieeexplore.ieee.org/abstract/document/8115245)
* [Real- time UAV weed scout for selective weed control by adaptive robust control and machine learning algorithm (2016)](https://elibrary.asabe.org/abstract.asp?aid=47088)


## 2019-11-21 Run03 - Manual sets

I've learned how to input in fastai the hand picked training/validation sets (instead of randomly selecting a 20% slice). Repeating the same procedure already used I obtain a sensibly worse error rate (31% vs. old 20-22%). This is caused by two factors:

* smaller training set
* classes relative proportions are different in the training set and in the validation set

I thus decided to create a new training/validation set, randomly choosing 20% of the total samples but respecting the classes proportions. For comparison, the following tables reports the proportion of classes in each set:

**Hand picked sets**

what | 1_no_infestation | 2_some_infestation | 3_high_infestation | 4_no_plants
----|-----------------|--------------------|--------------------|-------------
calibration | 39.68%   |      27.54%   |      26.59%    |     6.17% 
validation | 55.62%    |     26.12%    |     16.43%   |      1.81% 


**Balanced sets**

what | 1_no_infestation | 2_some_infestation | 3_high_infestation | 4_no_plants
----|-----------------|--------------------|--------------------|-------------
calibration | 47.69%     |     26.84%    |      21.47%      |    3.97%
validation | 47.64%     |    26.76%     |    21.54%   |      4.04%

**Samples in validation**

* Old set: 1485/2959 (50.19%)
* New set: 594/2959 (20.07%)

With the new set the best error rate was 20.71%, quite as expected. The comparison of the two confusion matrices confirms the case:

Hand picked sets | Balanced sets
-----------------|--------------- 
![alt text](results/run03/confmat.png "Confusion matrix for hand picked sets") | ![alt text](results/run03/confmat.balanced.png "Confusion matrix for balanced sets")

My suggestion is thus to keep working on the Balanced sets.

TODO

* (same as run02) test a class-4 only classifier, so that infestation measure can then become a scalar
* (same as run02)reach a consensus on data verification. We can either accept as it is, do one or more passes on the new "suspect" images or do another hand classification to find ambiguous images.


## 2019-10-30 Run02 - Classification updates

After the suspect samples discovered in Run01 I asked for a second pass of manual classification on the top 40 "suspect" samples, as described in the previous update.

I discovered that 25 out of 40 samples changed class, as described in the following confusion matrix:

.   | [1] | [2] | [3] | [4]
----|-----|-----|-----|-----
[1] |  7  |  3  |  0  |  0
[2] |  5  |  5  |  3  |  0
[3] |  0  |  8  |  3  |  0
[4] |  5  |  0  |  1  |  0

Rows indicate the class from first classification, dated 2019-10-18.
Columns indicate the class from the new classification, dated 2019-10-30.

As such, the "5" in position (4,1) indicates that 5 samples in the first classification were labelled as "4-no plants" and in the second they bacame "1-no infestation".

I've also collected the comparison for these 40 images in a [.csv file available here](data/batch_01_public/classi/nick_2019_10_30/comparison.csv) (there is a download button on the right).

I've decided to run again the same model I already tested on Run01, but with these updated classes. I've obtained a slightly better error rate (around 20%, compared to the 22% of the old run) and the following confusion matrix:

![alt text](results/run02/conf.png "Confusion matrix")

Things are getting better: we have 0 occurrencies of the worst case error (high infestation - no infestation), and in general when there is high infestation the system predicts either correct (108/127) or "low infestation". The 1-4 confusion (no plants, no infestation) is still there, though.

TODO

* (same as run01) use assigned validation set instead of random ones
* (same as run01) test a class-4 only classifier, so that infestation measure can then become a scalar
* reach a consensus on data verification. We can either accept as it is, do one or more passes on the new "suspect" images or do another hand classification to find ambiguous images.

## 2019-10-24 Run01 (naive)

I've run a first, naive classifier using Resnet34 and fasta.ai default parameters, just to get the general sense of the dataset. I did not use the hand-picked validation set, but simply put all the 2959 images together and created a random 20% slice for validation. (The reason is: I'm still new with the fast.ai library and this was the easies approach).

I ended up with the following accuracies:

![alt text](results/run01/accuracy.png "Accuracy")

So we are talking about 22% error rate, [not great, not terrible](https://www.youtube.com/watch?v=Mg5HOnq7zD0). Taking a look at the confusion matrix:

![alt text](results/run01/confusion_matrix.png "Confusion matrix")

We discover that the most confounded classes are 1 (no infestation) vs. 2 (some infestation). Also, there is a small but suspicious number of 1-4 confusion (maybe images from these classes are all homogeneous and thus similar?)

I've also taken a look to the samples with the biggest loss value (meaning the algorithm is very certain of a wrong label, or is very uncertain). I found the following image:

![alt text](results/run01/estrazione_5m__2678.jpg "Suspect")

Which is labeled as "3 - high infestation", while the algo predicts "1 - no infestation". To my (untrained) eye the image does not show high infestation, though. Could it be a human error?

To follow this I've prepared a list of the top 20 images from the random train set and the random validation set (thus: 40 images) ranked for biggest loss values. These "suspect" images could use a double check and [can be found here](https://gitlab.com/ne1s0n/crea-za_maize_satellite/tree/master/results/run01/suspect) (top right is a "download all" button).

TODO

* use assigned validation set instead of random ones
* test a class-4 only classifier, so that infestation measure can then become a scalar
* hand check for suspicious images to find classification errors


## 2019-10-18 First hand-labeled data
I received the first batch of hand-labeled data. They are divided in two sets for calibration and validation. Here the class tally:

| Set         | Class 1 | Class 2 | Class 3 | Class 4 | Total |
|-------------|---------|---------|---------|---------|-------|
| Calibration | 578  | 410 | 389 | 97  | 1474 |
| Validation  | 826  | 381 | 251 | 27  | 1485 |
| Total       | 1404 | 791 | 640 | 124 | 2959 |

This numerosity should be adequate for a starting experiment.

## 2019-09-18 Classifier helper

I produced a script in the form of an html file with embedded javascript to help streamline the manual classification of images. It is found in this repo under
[software/classifier_helper](https://gitlab.com/ne1s0n/crea-za_maize_satellite/tree/master/software/classifier_helper).

## 2019-09-17 Meeting

- defined RGB tassellated images of the field, each one representing a 5m by 5m square. We ended up with 3290 images. Images are 490x490 .tif, rotated by 11.141°, with transparent pixels (value -32768 signed)
- I wrote a [preprocessing script](https://gitlab.com/ne1s0n/crea-za_maize_satellite/tree/master/software/scripts/preprocess_01_rotated.py) that produces non-rotated, smaller .jpg pictures without the transparent pixels. Final dimension is 414x414.
- defined the four target classes for supervised learning:
    - [1] No infestation
    - [2] Some infestation
    - [3] High infestation
    - [4] No plants

## 2019-09-2 Repo is initiated

I decided to create a README.md in each subfolder to ease the compartimentation of technical information. Also, the project just started :)
